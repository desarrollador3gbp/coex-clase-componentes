import Configurar from './configurar/configurar'
import Product from './product/product'
export default [
    {
        path: '/',
        name: 'home',
        component: ()=>import('../pages/Home.vue').then(m=>m.default || m)
    },
    ...Configurar,
    ...Product
]
