export default [
    {
        path: '/product',
        name: 'product',
        redirect: {name: 'product.list'},
        component: () => import(/* webpackChunkName: "configurar" */ '../../pages/product').then(m => m.default || m),
        children: [
            {
                path: 'list',
                meta: { nombre: 'List products' },
                name: 'product.list',
                component: () => import(/* webpackChunkName: "configurar" */ '../../pages/product/list').then(m => m.default || m),
            },
        ]
    }
]