import Vue from 'vue'
import lodash from 'lodash'
import './vee-validate'
import './draggable'
import './slim'
// import moment from 'moment'
// moment.updateLocale('es', {
//     week: { dow: 0 }
// })
Vue.prototype._ = lodash
